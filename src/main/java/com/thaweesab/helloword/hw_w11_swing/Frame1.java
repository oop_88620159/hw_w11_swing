/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w11_swing;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author acer
 */
public class Frame1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,300);
        
        JLabel lblHelloWorld = new JLabel("Hello World!",JLabel.CENTER); 
        lblHelloWorld.setBackground(Color.green);
        lblHelloWorld.setOpaque(true);
        lblHelloWorld.setFont(new Font("Serif", Font.BOLD, 50));
        frame.add(lblHelloWorld);
        
    
        frame.setVisible(true);
        
        
    }
}
