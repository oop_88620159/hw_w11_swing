/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w11_swing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
class MyActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("ActionListener: Action");
    }
    
}
/**
 *
 * @author acer
 */
public class HelloMe implements ActionListener{
    public static void main(String[] args) {
        JFrame frmMain = new JFrame("Hello Me");
       
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmMain.setSize(500,300);
        
        JLabel lblYouName = new JLabel ("You Name");
        lblYouName.setSize(80, 20);
        lblYouName.setLocation(5, 5);
        lblYouName.setBackground(Color.white);
        lblYouName.setOpaque(true);
       
        JTextField txtYouName = new JTextField();
        txtYouName.setSize(200, 20);
        txtYouName.setLocation(90,5);
        
        JButton btnHello = new JButton("Hello");
        btnHello.setSize(80, 20);
        btnHello.setLocation(80, 40);
       
        MyActionListener myActionListener = new MyActionListener();
        btnHello.addActionListener(myActionListener);
        btnHello.addActionListener(new HelloMe());
                
                
        
        JLabel lblHello = new JLabel("Hello...",JLabel.CENTER);
        lblHello.setSize(200,20);
        lblHello.setLocation(90,70);
        lblHello.setBackground(Color.white);
        lblHello.setOpaque(true);
        
        frmMain.setLayout(null);
        
        frmMain.add(lblYouName);
        frmMain.add(txtYouName);
        frmMain.add(btnHello);
        frmMain.add(lblHello);
                
        btnHello.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                 String message = txtYouName.getText();
                 lblHello.setText("Hello "+message);
            }
            
        });
        
        frmMain.setVisible(true);
                
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Hello Me: ActionListener");
    }
}
